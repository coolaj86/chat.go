# chat.go

Rudimentary go chat server as a fun project.

# Install

```bash
git clone https://git.coolaj86.com/coolaj86/chat.go.git

go get gopkg.in/yaml.v2
go get github.com/emicklei/go-restful
```

Note: I also copied some code directly from
<https://github.com/polvi/sni/blob/master/sni.go>

# Usage

**Start the server**

```bash
go run -race chatserver*.go -conf ./config.yml
```

See sample config file at `config.sample.yml`.

**Connect clients**

You can connect multiple clients.

```bash
telnet localhost 4080
```

You can also use HTTP.

```
curl http://localhost:4080
```

## Testing Beyond Localhost

You can use [Telebit](https://telebit.cloud) to share the experience with friends and loved ones:

Completely userspace installation:

```
curl -fsSL https://get.telebit.cloud | bash
```

Easy configuration:

```
~/telebit http 4080
> Forwarding https://lucky-duck-42.telebit.fun => localhost:4080

~/telebit tcp 4080
> Forwarding telebit.cloud:1234 => localhost:4080
```

Caveat: Due to a bug in telebit you must send 'hello' in telnet to establish a connection.

# API Docs

The API docs and examples can be seen at <http://localhost:4080>

# Project Approach

I've understood theoretical principles of Go for a long time and I've always loved it.
However, most of that came from watching Go Tech Talks and going to meetups.

This is my first Go project and my primary goal was to learn how to use Go
for the kinds of things that I'm personally interested in while also satisfying
a mix of the requirements and optional add-ons, and show you that I know how
to write code and learn.

Criteria Met
-----

* [x] Works
* [x] Attention to Detail
  * [x] Security
  * [x] UX
  * [x] Performance
* [x] Commenting
* [x] Creativity

Limitations
----------

* [ ] Good coding style

This is my first Go project so I was learning as I went and trying different approaches.
As a result my code style is inconsistent and probably does some things the wrong way
(especially confusing since I probably did it the right way in other places).

Implemented
-----

* [x] Awesome telnet server
  * [x] Multiple clients can connect
  * [x] Messages are relayed to all clients
  * [x] Includes timestamp, name of client
  * [x] Config file for port and addr
* [x] HTTP API
  * [x] Post message
  * [x] List messages
  * [x] Serve Docs
  * [x] Working curl examples
* [x] Multiplex the same port (because I wanted to learn)
* [x] E-mail "magic link" authentication (minus the link since it's localhost)

Not Implemented
----

I don't think these things would be difficult to add,
but I was having fun learning lots of other things
and I figured they're not that important.

* [ ] local log file
* [ ] Rooms
* [ ] Blocking
* [ ] UI for HTTP API
